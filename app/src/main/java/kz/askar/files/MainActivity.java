package kz.askar.files;

import android.app.DownloadManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.media.ImageReader;
import android.os.Build;
import android.os.Environment;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.Volley;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

public class MainActivity extends AppCompatActivity {

    File ourFolder;
    RequestQueue queue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        queue = Volley.newRequestQueue(this);

        File picturesFolder;
        if(Build.VERSION.SDK_INT>=19){
            picturesFolder = getExternalFilesDir("Pictures");
        }else{
            picturesFolder = new
                    File(Environment.getExternalStorageDirectory()
                    .getAbsolutePath()+"/Pictures");
        }

        ourFolder = new File(picturesFolder+"/sdu");
        if(!ourFolder.exists()){
            ourFolder.mkdirs();
        }
        Log.d("My", ourFolder.listFiles().length+"");
    }

    public void download(View v){
        ImageRequest request = new ImageRequest("https://cdn.pixabay.com/photo/2016/07/23/00/12/sun-flower-1536088_960_720.jpg", new Response.Listener<Bitmap>() {
            @Override
            public void onResponse(Bitmap response) {
                Log.d("My", "Here");
                File file = new File(ourFolder.getAbsolutePath() + "/flower1.jpg");
                FileOutputStream outStream = null;
                try {
                    outStream = new FileOutputStream(file);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                response.compress(Bitmap.CompressFormat.JPEG, 100, outStream);
                try {
                    outStream.flush();
                    outStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }, 0, 0, null, null, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("My", error.toString());
            }
        });
        queue.add(request);

//        try {
//            openFileOutput("file", MODE);
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        }
    }

    public void show(View v){
        Log.d("My", ourFolder.listFiles().length+"");
        for(File file:ourFolder.listFiles()){
            Log.d("My", file.getName()+" "+file.getAbsolutePath()+" "+file.isDirectory());
        }


        File imageFile = ourFolder.listFiles()[0];
        Bitmap bitmap = BitmapFactory.decodeFile(imageFile.getAbsolutePath(), null);
        ImageView iv = (ImageView)findViewById(R.id.iv);
        iv.setImageBitmap(bitmap);
    }
}
