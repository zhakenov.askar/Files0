package kz.askar.files_group2;

import android.app.DownloadManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.Volley;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.SQLDataException;

public class MainActivity extends AppCompatActivity {

    File picturesFolder;
    File ourFolder;
    RequestQueue queue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        queue = Volley.newRequestQueue(this);

        if (Build.VERSION.SDK_INT >= 19) {
            picturesFolder = getExternalFilesDir("Pictures");
        } else {
            picturesFolder = new File(Environment.getExternalStorageDirectory()
                    .getAbsolutePath() + "/Pictures");
        }

        ourFolder = new File(picturesFolder.getAbsolutePath()+"/dogs/puppies");
        if(!ourFolder.exists()){
            ourFolder.mkdirs();
        }

        downloadPhoto(null);
    }


    public void downloadPhoto(View v){
        ImageRequest request = new ImageRequest("https://images-na.ssl-images-amazon.com/images/G/01/img15/pet-products/small-tiles/23695_pets_vertical_store_dogs_small_tile_8._CB312176604_.jpg",
                new Response.Listener<Bitmap>() {
                    @Override
                    public void onResponse(Bitmap response) {
                        File file = new File(ourFolder.getAbsolutePath()+"/dog1.jpg");
                        try {
                            FileOutputStream outputStream = new FileOutputStream(file);
                            response.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
                            try {
                                outputStream.flush();
                                outputStream.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }
                    }
                }, 0, 0, null, null, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        queue.add(request);
    }

    public void show(View v) {
        ImageView iv = (ImageView)findViewById(R.id.iv);

        File imageFile = ourFolder.listFiles()[0];
        Bitmap bitmap = BitmapFactory.decodeFile(imageFile.getAbsolutePath(), null);
        iv.setImageBitmap(bitmap);

        Log.d("My", ourFolder.getAbsolutePath() + " " + ourFolder.listFiles().length);
        for (File file : ourFolder.listFiles()) {
            Log.d("My", file.getName() + " " + file.getAbsolutePath() + " " + file.isDirectory());
        }
    }
}
